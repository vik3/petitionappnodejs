# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 37.139.30.27 (MySQL 5.7.19-0ubuntu0.16.04.1)
# Database: petition
# Generation Time: 2017-09-22 14:41:37 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table persons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `persons`;

CREATE TABLE `persons` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `petitionID` int(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `petitionID` (`petitionID`) USING BTREE,
  CONSTRAINT `persons_ibfk_1` FOREIGN KEY (`petitionID`) REFERENCES `petitions` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

LOCK TABLES `persons` WRITE;
/*!40000 ALTER TABLE `persons` DISABLE KEYS */;

INSERT INTO `persons` (`ID`, `firstName`, `lastName`, `email`, `gender`, `petitionID`)
VALUES
	(1,'Viking','Berglind','viking.berglind@gmail.com','male',3),
	(2,'Karl','Larsson','Karl.Larsson@hotmail.com','male',3),
	(3,'ulrica','magnifica','ullis@hawaii.com','female',3),
	(4,'KlasGoran','Ulfberg','KG.Ulf@bajenpartiet.se','male',2),
	(5,'Sven','Berglind','sven@hotmail.com','male',2),
	(6,'Pierre','Plast','Pierre@tippen.se','male',4),
	(7,'Kuf','Kufsson','Kufzor@venus.com','other',4),
	(8,'Max','Olderius','Trax@freddan.nu','male',3),
	(9,'Noel','Berglind','nossi@msn.se','male',3),
	(10,'Olle','Svensson','o@hotmail.com','male',3),
	(11,'Helena','Marteus','HM@gmail.com','female',3);

/*!40000 ALTER TABLE `persons` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table petitions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `petitions`;

CREATE TABLE `petitions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

LOCK TABLES `petitions` WRITE;
/*!40000 ALTER TABLE `petitions` DISABLE KEYS */;

INSERT INTO `petitions` (`ID`, `title`, `description`)
VALUES
	(2,'Kall bira','Kall bira till nattvarden'),
	(3,'Bevara högalidshallen','Som det låter, stans bästa affär ska stänga, stoppa gentrifieringen!'),
	(4,'Förbjud pappersreklam','Förbjud all form av pappersreklam. ');

/*!40000 ALTER TABLE `petitions` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
