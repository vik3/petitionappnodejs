var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var fs = require('fs');

var config = require('../globals.js');


/* GET home page. */
router.get('/', function (req, res, next) {

   var pool = mysql.createPool(config.database);

   pool.getConnection((err, connection) => {
      if (err) throw err;
      connection.query('SELECT * FROM petitions', function (error, rows, fields) {
         if (error) {
            console.log(error);
            connection.release();
         } else {
            connection.release();
            res.render('index', {
               title: 'namninsamling.en',
               data: rows,
            });

         }
      });
   });
});


module.exports = router;