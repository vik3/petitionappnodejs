var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var fs = require('fs');

var config = require('../globals.js');




/* GET petition page. */
router.get('/:id-:admin', function (req, res, next) {
   var pool = mysql.createPool(config.database);
   var isAdmin = req.params.admin;

   var return_data = {};
   req.params.id;
   //fetching data for petition
   async.parallel([
      function (parallel_done) {

         pool.query("SELECT * FROM persons WHERE petitionID = ? ", [req.params.id], function (err, results) {
            if (err) return parallel_done(err);
            return_data.persons = results;
            parallel_done();
         });
      },
      function (parallel_done) {
         pool.query("SELECT * FROM petitions WHERE ID = ? ", [req.params.id], function (err, results) {
            if (err) return parallel_done(err);
            return_data.petition = results;
            parallel_done();
         });
      }
   ], function (err) {
      if (err) console.log(err);
      pool.end();
      res.render('petition', {
         title: 'namninsamling.en',
         data: return_data,
         admin: isAdmin
      });
   });
});

router.post('/send/:id', function (req, res) {

   var validData = false;
   if (checkName(req.body.fname_field) && checkName(req.body.lname_field) && checkEmail(req.body.email_field) && checkGender(req.body.gender)) {
      validData = true;
   }
   //check wether the input data is valid
   if (!validData) {
      res.send("1");
   } else {
      var pet_id = req.params.id;
      var pool = mysql.createPool(config.database);
      pool.getConnection((err, connection) => {
         if (err) throw err;
         connection.query("SELECT email FROM persons WHERE email = ? AND petitionID = ?", [req.body.email_field, pet_id], function (error, rows, fields) {

            if (error) {
               console.log(error);
               connection.release();
            } else {
               //Checks whether the person already exists in current petition
               if (rows.length > 0) {
                  connection.release();
                  res.send("0");

               } else {
                  //  INERT person to database
                  connection.query("INSERT INTO persons (firstName, lastName, email, gender, petitionID) VALUES (?,?,?,?,?)", [req.body.fname_field, req.body.lname_field, req.body.email_field, req.body.gender, pet_id], function (err, result) {
                     if (err) throw err;
                  });

                  connection.release();
                  //sends back name to instant display the signature to the user
                  res.send([req.body.fname_field, req.body.lname_field]);
               }
            }

         });
      });
   }
});


//help functions to check the validity of input using regex
function checkName(name) {
   if (name.search(/[^a-zA-ZäöåÄÖÅ]+/) === -1 && name.length >= 1) {
      return true;

   } else {
      return false;
   }

}

function checkEmail(email) {
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   if (!reg.test(email))
      return false;

   return true;
}

function checkGender(gender) {
   if (gender == "male" || gender == "female" || gender == "other") {
      return true;
   } else
      return false;
}


module.exports = router;